
CC=gcc
FLAGS=-std=c99 -O2 -Wall -pedantic

all: primes primes-inline

clean:
	rm *o

primes: primes.c eratosthenes.c eratosthenes.c bit_array.h
	$(CC) $(CFLAGS) -o primes primes.c 2>/dev/null

primes-inline: primes.c eratosthenes.c bit_array.h
	$(CC) $(CFLAGS) -DUSE_INLINE primes.c -o primes-inline 2>/dev/null