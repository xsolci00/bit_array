/*
 * eratosthenes.c
 *
 *  Created on: 18.3.2016
 *      Author: Vit Solcik (xsolci00), FIT VUT Brno
 */

//#include <stdio.h>
//#include <stdlib.h>
//#include <string.h>
//#include "bit_array.h"

//eclipse testing nesessery...
//#include "bit_array.h"



void extern Eratosthenes(bit_array_t pole){
	unsigned long int i = 2;
	unsigned long int j;
	unsigned long int n = ba_size(pole);


//	for(i = 2; i <= n ;i++)
//		DU1_SET_BIT_(pole,i,0);

	for(i = 2; i <= n; i++){
			for(j = 2; (i*j) <= n;j++){
				if (DU1_GET_BIT_(pole,i*j) == 0)
					DU1_SET_BIT_(pole,i*j,1);
			}
	}


	long unsigned int x;
	long unsigned int result[10];
	long unsigned int prime_count = 0;
    for (x=n; prime_count < 10;x--){
        if (DU1_GET_BIT_(pole,x) == 0){
//           printf("%lu\n",x);
            result[prime_count] = x;
            prime_count++;
            }
    }
    int z;
    for(z=9; z >= 0; z--){
    	printf("%lu\n",result[z]);
    }
}

