/*
 * bit_array.h
 *
 *  Created on: 18.3.2016
 *      Author: Vit Solcik (xsolci00), FIT VUT Brno
 */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>


typedef unsigned long int bit_array_t[];


void static Eratosthenes(bit_array_t pole);

#define ba_create(jmeno_pole,size)\
		unsigned long int count;\
		unsigned long int velikost = size;\
		count = (velikost  / (sizeof(long int)*8)) + 2;\
		if((velikost % (sizeof(long int)*8)) == 0)\
			count --;\
		unsigned long int jmeno_pole[count];\
		memset(jmeno_pole,0,sizeof(long int)*count);\
		jmeno_pole[0]=velikost;

////////////////////      MAKRA      /////////////////////////
#ifndef USE_INLINE
#define ba_size(jmeno_pole) ({\
		unsigned long int size = jmeno_pole[0];\
		size;\
		})

#define ba_set_bit(jmeno_pole,index,vyraz){\
		unsigned long int delka_check = ba_size(jmeno_pole);\
		if(index > delka_check) return -1;\
		unsigned long int block = index/(sizeof(long int) * 8);\
		unsigned long int position = index % (sizeof(long int) * 8);\
		if(vyraz == 0)\
			jmeno_pole[block+1] &= ~(1UL << position);\
		else\
			jmeno_pole[block+1] |= 1UL << (position);}
//todo misto reset -1 errfce

#define ba_get_bit(jmeno_pole, index)({\
		unsigned long int delka_check2 = ba_size(jmeno_pole);\
		if(index > delka_check2) return -1;\
		unsigned long int block2 = index/(sizeof(long int) * 8);\
		unsigned long int position2 = index % (sizeof(long int) * 8);\
		unsigned long int mask = 1UL << position2;\
		unsigned long int result = jmeno_pole[block2+1] & mask;\
		result = result >> position2;\
		result;\
		})
//todo misto reset -1 errfce
#endif

#define DU1_SET_BIT_(p,i,b){\
		unsigned long int block = i/(sizeof(long int) * 8);\
		unsigned long int position = i % (sizeof(long int) * 8);\
		if(b == 0)\
			p[block+1] &= ~(1UL << position);\
		else\
			p[block+1] |= 1UL << (position);\
		}

#define DU1_GET_BIT_(p,i)({\
		unsigned long int block2 = i/(sizeof(long int) * 8);\
		unsigned long int position2 = i % (sizeof(long int) * 8);\
		unsigned long int mask = 1UL << position2;\
		unsigned long int result = p[block2+1] & mask;\
		result = result >> position2;\
		result;\
		})


#ifdef USE_INLINE
inline static long int ba_get_bit(bit_array_t jmeno_pole,long int index){
//		if(index > ba_size(jmeno_pole)) return -1;
		unsigned long int block = index/ (sizeof(long int) * 8);
		unsigned long int possition = index % (sizeof(long int) * 8);
		long unsigned int mask = 0;
		mask = 1UL << possition;
		unsigned long int result = jmeno_pole[block+1] & mask;
		result = result >> possition;
		return result;

}


inline static void ba_set_bit(bit_array_t jmeno_pole,long int index,int vyraz){
//		if(index > ba_size(jmeno_pole)) {exit;}
		unsigned long int block = index/(sizeof(long int) * 8);
		unsigned long int position = index % (sizeof(long int) * 8);
		if(vyraz == 0)
			jmeno_pole[block+1] &= ~(1UL << position);
		else
			jmeno_pole[block+1] |= 1UL << (position);
}

inline static long int ba_size(bit_array_t jmeno_pole){
		unsigned long int size = jmeno_pole[0];
		return size;
}
#endif

